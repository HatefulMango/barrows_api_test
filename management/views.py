from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse, Http404

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status, permissions

from . models import Client, Project
from . serializers import clientSerializer, projectSerializer

class clientList(APIView):

    permission_classes = [permissions.IsAuthenticated]

    #List clients or add a new client
    def get(self, request):
        clients = Client.objects.filter(client_deleted = False)
        serializer = clientSerializer(clients, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = clientSerializer(data = request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status = status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST)

class clientDetail(APIView):
    
    permission_classes = [permissions.IsAuthenticated]

    #Read, update, or delete/deactivate a single client
    def get_client(self, pk):
        try:
            return Client.objects.get(pk = pk)
        except Client.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        client = self.get_client(pk)
        serializer = clientSerializer(client)
        return Response(serializer.data)

    def put(self, request, pk):
        client = self.get_client(pk)
        serializer = clientSerializer(client, data = request.data, partial = True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        client = self.get_client(pk)

        data = request.data
        client.client_deleted = data.get('client_deleted', True)
   
        client.save()

        return Response(status = status.HTTP_204_NO_CONTENT)

class projectList(APIView):

    permission_classes = [permissions.IsAuthenticated]

    #List projects or add a new project
    def get(self, request):
        projects = Project.objects.filter(project_deleted = False)
        serializer = projectSerializer(projects, many=True)
        return Response(serializer.data)

    def post(self, request):
        serializer = projectSerializer(data = request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status = status.HTTP_201_CREATED)
        else:
            return Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST)

class projectDetail(APIView):

    permission_classes = [permissions.IsAuthenticated]

    #Read, update, or delete a single project
    def get_project(self, pk):
        try:
            return Project.objects.get(pk = pk)
        except Project.DoesNotExist:
            raise Http404

    def get(self, request, pk):
        project = self.get_project(pk)
        serializer = projectSerializer(project)
        return Response(serializer.data)
        
    def put(self, request, pk):
        project = self.get_project(pk)
        serializer = projectSerializer(project, data = request.data, partial = True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        else:
            return Response(serializer.errors, status = status.HTTP_400_BAD_REQUEST)

    def delete(self, request, pk):
        project = self.get_project(pk)

        data = request.data
        project.project_deleted = data.get('project_deleted', True)
   
        project.save()

        return Response(status = status.HTTP_204_NO_CONTENT)


    
    
