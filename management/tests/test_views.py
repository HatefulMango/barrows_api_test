import pytest, mock, requests

from django.contrib.auth.models import User
from django.urls import resolve, reverse
from django.utils import timezone

from management.models import Client, Project
from management import views as m_views

from rest_framework.test import APIClient
from rest_framework_simplejwt.tokens import RefreshToken

class TestViews():

    @pytest.fixture
    def api_client(self):
        user = User.objects.create_user(username='john', email='js@js.com', password='js.sj')
        client = Client.objects.create(id=1, client_id="c0001", client_name="Macro", client_address="69 Road Street", date_created=timezone.now(), date_updated=timezone.now(), client_deleted=False)
        api_client = APIClient()
        refresh = RefreshToken.for_user(user)
        api_client.credentials(HTTP_AUTHORIZATION=f'Bearer {refresh.access_token}')
        
        return api_client


    @pytest.mark.django_db(transaction=True)
    def test_unauthorized_client_request(self):
        url = reverse('client_list')
        api_client = APIClient()
        response = api_client.get(url)
        assert response.status_code == 401

    @pytest.mark.django_db(transaction=True)
    def test_authorized_client_list_request(self, api_client):
        url = reverse('client_list')


        response = api_client.get(url)

        print(response.content)
        assert response.status_code == 200

    @pytest.mark.django_db(transaction=True)
    def test_authorized_project_post(self, api_client):
        url = reverse('project_list')

        api_client.post(url, {"project_id":"p0007","client_id":2,"project_name":"stuff 7","project_desc":"a few stuff more","date_created":timezone.now(),"date_updated":timezone.now(),"project_deleted":False}, format='json')

        response = api_client.get(url)
        print(response.content)

    @pytest.mark.django_db(transaction=True)
    def test_authorized_client_detail_request(self, api_client):
        url = reverse('client_detail', kwargs={'pk': 1})
        
        

        response = api_client.get(url)
        print(response.content)
        assert response.status_code == 200
