from django.urls import reverse, resolve

class TestUrls:

    def test_client_list_url(self):
        path = reverse('client_list')
        assert resolve(path).view_name == 'client_list'

    def test_client_detail_url(self):
        path = reverse('client_detail', kwargs={'pk': 1})
        assert resolve(path).view_name == 'client_detail'

    def test_project_list_url(self):
        path = reverse('project_list')
        assert resolve(path).view_name == 'project_list'

    def test_project_detail_url(self):
        path = reverse('project_detail', kwargs={'pk': 1})
        assert resolve(path).view_name == 'project_detail'