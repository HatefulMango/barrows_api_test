from rest_framework import serializers
from . models import Client, Project

class clientSerializer(serializers.ModelSerializer):

    class Meta:
        model = Client
        fields = ['client_id', 'client_name', 'client_address', 'date_created', 'date_updated', 'client_deleted']

class projectSerializer(serializers.ModelSerializer):

    class Meta:
        model = Project
        fields = ['project_id', 'client_id', 'project_name', 'project_desc', 'date_created', 'date_updated', 'project_deleted']