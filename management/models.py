from django.db import models

# Create your models here.

class Client(models.Model):
    client_id = models.CharField(max_length=20)
    client_name = models.CharField(max_length=100)
    client_address = models.CharField(max_length=200)
    date_created = models.DateTimeField('date created')
    date_updated = models.DateTimeField('date updated')
    client_deleted = models.BooleanField(default=False)

    class Meta:
        ordering = ['date_created']



class Project(models.Model):
    project_id = models.CharField(max_length=20)
    client_id = models.ForeignKey(Client, on_delete=models.CASCADE)
    project_name = models.CharField(max_length=100)
    project_desc = models.TextField()
    date_created = models.DateTimeField('date created')
    date_updated = models.DateTimeField('date updated')
    project_deleted = models.BooleanField(default=False)

    class Meta:
        ordering = ['date_created']
