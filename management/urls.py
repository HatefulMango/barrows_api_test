from django.urls import path, include
from . import views

urlpatterns = [
    path('client/', views.clientList.as_view(), name='client_list'),
    path('client/<int:pk>/', views.clientDetail.as_view(), name='client_detail'),
    path('project/', views.projectList.as_view(), name='project_list'),
    path('project/<int:pk>/', views.projectDetail.as_view(), name='project_detail'),

]
